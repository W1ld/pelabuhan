<?php

use Illuminate\Support\Facades\{Route, Auth};
use App\Http\Controllers\HomeController;
use App\Http\Controllers\dataController;
use App\Http\Controllers\stevedoringController;
use App\Http\Controllers\drController;
use App\Http\Controllers\cargodoringController;

Auth::routes();

Route::get('/', HomeController::class)->name('home');
Route::get('/data', dataController::class)->name('data');
Route::get('/stevedoring', stevedoringController::class)->name('stevedoring');
Route::get('/dr', drController::class)->name('dr');
Route::get('/cargodoring', cargodoringController::class)->name('cargodoring');

Route::get('/delete/{id}', [HomeController::class,'delete']);

Route::get('/data/add', [dataController::class,'add']);
Route::post('/data/insert', [dataController::class,'insert']);
// Route::get('/data/edit/{id}', [dataController::class,'edit']);
// Route::post('/data/save/{id}', [dataController::class,'save']);
// Route::get('/data/delete/{id}', [dataController::class,'delete']);

// Route::get('/stevedoring/add', [stevedoringController::class,'add']);
// Route::post('/stevedoring/insert', [stevedoringController::class,'insert']);
Route::get('/stevedoring/edit/{id}', [stevedoringController::class,'edit']);
Route::post('/stevedoring/save/{id}', [stevedoringController::class,'save']);
// Route::get('/stevedoring/delete/{id}', [stevedoringController::class,'delete']);

// Route::get('/dr/add', [drController::class,'add']);
// Route::post('/dr/insert', [drController::class,'insert']);
Route::get('/dr/edit/{id}', [drController::class,'edit']);
Route::post('/dr/save/{id}', [drController::class,'save']);
// Route::get('/dr/delete/{id}', [drController::class,'delete']);

// Route::get('/cargodoring/add', [cargodoringController::class,'add']);
// Route::post('/cargodoring/insert', [cargodoringController::class,'insert']);
Route::get('/cargodoring/edit/{id}', [cargodoringController::class,'edit']);
Route::post('/cargodoring/save/{id}', [cargodoringController::class,'save']);
// Route::get('/cargodoring/delete/{id}', [cargodoringController::class,'delete']);
