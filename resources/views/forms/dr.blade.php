@extends('view.template')
@section('title','Edit Data')
@section('content')

<form action="/dr/save/{{$transaksi['id']}}" method="post">
    @csrf
    <div class="row d-flex justify-content-center" style="width: 80vw;padding-top:7vh">
        <div class="col-4 ms-5 me-5">
            <label for="id">ID</label>
            <input type="text" class="form-control" value="{{$transaksi['id']}}" name="id" id="id" disabled><br>
            <label for="tipe">Tipe Transaksi</label>
            <select name="tipe" name="tipe" id="tipe" class="form-select" disabled>
                <option <?php if ($transaksi['tipe'] == "Bongkar") echo "selected" ?> value="Bongkar">Bongkar</option>
                <option <?php if ($transaksi['tipe'] == "Muat") echo "selected" ?> value="Muat">Muat</option>
            </select><br>
            <label for="nomor_imo">Nomor IMO</label>
            <input type="text" class="form-control" value="{{$transaksi['nomor_imo']}}" name="nomor_imo" id="nomor_imo"><br>
            <label for="nama_perusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" value="{{$transaksi['nama_perusahaan']}}" name="nama_perusahaan" id="nama_perusahaan"><br>
            <label for="tanggal_stevedoring">Tanggal Stevedoring</label>
            <input type="date" class="form-control" value="{{$transaksi['tanggal_stevedoring']}}" name="tanggal_stevedoring" id="tanggal_stevedoring"><br>
            <label for="jam_stevedoring">Jam Stevedoring</label>
            <input type="time" class="form-control" value="{{$transaksi['jam_stevedoring']}}" name="jam_stevedoring" id="jam_stevedoring"><br>
            <br>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a class="btn btn-danger ms-5" href="/dr" role="button">Batal</a>
        </div>
        <div class="col-4 ms-5">
            <label for="jumlah_kontainer">Jumlah Kontainer</label>
            <input type="text" class="form-control" value="{{$transaksi['jumlah_kontainer']}}" name="jumlah_kontainer" id="jumlah_kontainer"><br>
            <label for="status_stevedoring">Status Stevedoring</label>
            <select name="status_stevedoring" id="status_stevedoring" class="form-select">
                <option value=""></option>
                <option <?php if ($transaksi['status_stevedoring'] == "IN PROGRESS") echo "selected" ?> value="IN PROGRESS">IN PROGRESS</option>
                <option <?php if ($transaksi['status_stevedoring'] == "FINISH") echo "selected" ?> value="FINISH">FINISH</option>
                <option <?php if ($transaksi['status_stevedoring'] == "ABORT") echo "selected" ?> value="ABORT">ABORT</option>
            </select><br>
            <label for="tanggal_dr">Tanggal Delivery / Receive</label>
            <input type="date" class="form-control" value="{{$transaksi['tanggal_dr']}}" name="tanggal_dr" id="tanggal_dr"><br>
            <label for="jam_dr">Jam Delivery / Receive</label>
            <input type="time" class="form-control" value="{{$transaksi['jam_dr']}}" name="jam_dr" id="jam_dr"><br>
            <label for="status_dr">Status Delivery / Receive</label>
            <select name="status_dr" id="status_dr" class="form-select">
                <option value=""></option>
                <option <?php if ($transaksi['status_dr'] == "IN PROGRESS") echo "selected" ?> value="IN PROGRESS">IN PROGRESS</option>
                <option <?php if ($transaksi['status_dr'] == "FINISH") echo "selected" ?> value="FINISH">FINISH</option>
                <option <?php if ($transaksi['status_dr'] == "ABORT") echo "selected" ?> value="ABORT">ABORT</option>
            </select>
        </div>
    </div>
</form>

@endsection