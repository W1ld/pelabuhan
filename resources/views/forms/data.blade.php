@extends('view.template')
@section('title','Tambah Data')
@section('content')

<form action="/data/insert" method="post">
    @csrf
    <div class="row d-flex justify-content-center" style="width: 80vw;padding-top:7vh">
        <div class="col-5 pe-5">
            <label for="id">ID</label>
            <input type="text" class="form-control" name="id" id="id" disabled><br>
            <label for="tipe">Tipe Transaksi</label>
            <select name="tipe" name="tipe" id="tipe" class="form-select" autofocus>
                <option value=""></option>
                <option value="Bongkar">Bongkar</option>
                <option value="Muat">Muat</option>
            </select><br>
            <label for="nomor_imo">Nomor IMO</label>
            <input type="text" class="form-control" placeholder="Nomor IMO" name="nomor_imo" id="nomor_imo" required><br>
            <label for="nama_perusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama_perusahaan" id="nama_perusahaan" required><br>
            <label for="tanggal_stevedoring">Tanggal Stevedoring</label>
            <input type="date" class="form-control" placeholder="" name="tanggal_stevedoring" id="tanggal_stevedoring"><br>
            <label for="jam_stevedoring">Jam Stevedoring</label>
            <input type="time" class="form-control" placeholder="" name="jam_stevedoring" id="jam_stevedoring"><br>
            <label for="jumlah_kontainer">Jumlah Kontainer</label>
            <input type="text" class="form-control" placeholder="Jumlah Kontainer" name="jumlah_kontainer" id="jumlah_kontainer"><br>
            <br>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a class="btn btn-danger ms-5" href="/data" role="button">Batal</a>
        </div>
        <div class="col-5 ps-5">
            <label for="status_stevedoring">Status Stevedoring</label>
            <select name="status_stevedoring" id="status_stevedoring" class="form-select">
                <option value=""></option>
                <option value="IN PROGRESS">IN PROGRESS</option>
                <option value="FINISH">FINISH</option>
                <option value="ABORT">ABORT</option>
            </select><br>
            <label for="lokasi_cy">Lokasi CY</label>
            <input type="text" class="form-control" placeholder="Lokasi CY" name="lokasi_cy" id="lokasi_cy"><br>
            <label for="status_cargodoring">Status Cargodoring</label>
            <select name="status_cargodoring" id="status_cargodoring" class="form-select">
                <option value=""></option>
                <option value="IN PROGRESS">IN PROGRESS</option>
                <option value="FINISH">FINISH</option>
                <option value="ABORT">ABORT</option>
            </select><br>
            <label for="tanggal_dr">Tanggal Delivery / Receive</label>
            <input type="date" class="form-control" placeholder="" name="tanggal_dr" id="tanggal_dr"><br>
            <label for="jam_dr">Jam Delivery / Receive</label>
            <input type="time" class="form-control" placeholder="" name="jam_dr" id="jam_dr"><br>
            <label for="status_dr">Status Delivery / Receive</label>
            <select name="status_dr" id="status_dr" class="form-select">
                <option value=""></option>
                <option value="IN PROGRESS">IN PROGRESS</option>
                <option value="FINISH">FINISH</option>
                <option value="ABORT">ABORT</option>
            </select>
        </div>
    </div>
</form>

@endsection