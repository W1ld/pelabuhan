@extends('view.template')
@section('title','Edit Data')
@section('content')

<form action="/cargodoring/save/{{$transaksi['id']}}" method="post">
    @csrf
    <div class="row d-flex justify-content-center" style="width: 80vw;padding-top:7vh">
        <div class="col-4 ms-5 me-5">
            <label for="id">ID</label>
            <input type="text" class="form-control" value="{{$transaksi['id']}}" name="id" id="id" disabled><br>
            <label for="tipe">Tipe Transaksi</label>
            <select name="tipe" name="tipe" id="tipe" class="form-select" disabled>
                <option <?php if ($transaksi['tipe'] == "Bongkar") echo "selected" ?> value="Bongkar">Bongkar</option>
                <option <?php if ($transaksi['tipe'] == "Muat") echo "selected" ?> value="Muat">Muat</option>
            </select><br>
            <label for="nomor_imo">Nomor IMO</label>
            <input type="text" class="form-control" disabled value="{{$transaksi['nomor_imo']}}" name="nomor_imo" id="nomor_imo"><br>
            <br>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a class="btn btn-danger ms-5" href="/stevedoring" role="button">Batal</a>
        </div>
        <div class="col-4 ms-5">
            <label for="lokasi_cy">Lokasi CY</label>
            <input type="text" class="form-control" value="{{$transaksi['lokasi_cy']}}" name="lokasi_cy" id="lokasi_cy"><br>
            <label for="status_cargodoring">Status Cargodoring</label>
            <select name="status_cargodoring" id="status_cargodoring" class="form-select">
                <option value=""></option>
                <option <?php if ($transaksi['status_cargodoring'] == "IN PROGRESS") echo "selected" ?> value="IN PROGRESS">IN PROGRESS</option>
                <option <?php if ($transaksi['status_cargodoring'] == "FINISH") echo "selected" ?> value="FINISH">FINISH</option>
                <option <?php if ($transaksi['status_cargodoring'] == "ABORT") echo "selected" ?> value="ABORT">ABORT</option>
            </select>
        </div>
    </div>
</form>

@endsection