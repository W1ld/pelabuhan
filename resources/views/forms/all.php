@extends('view.template')
@section('content')

@if(isset($transaksi))
@section('title','Edit Data')
<form action="/save/{{$transaksi['id']}}" method="post">
    @csrf
    <div class="row d-flex justify-content-center" style="width: 80vw;padding-top:7vh">
        <div class="col-4 ms-5 me-5">
            <label for="tipe">Tipe Transaksi</label>
            <input type="text" class="form-control" value="{{$transaksi['tipe']}}" name="tipe" id="tipe"><br>
            <label for="nomor_imo">Nomor IMO</label>
            <input type="text" class="form-control" value="{{$transaksi['nomor_imo']}}" name="nomor_imo" id="nomor_imo"><br>
            <label for="nama_perusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" value="{{$transaksi['nama_perusahaan']}}" name="nama_perusahaan" id="nama_perusahaan"><br>
            <label for="tanggal_stevedoring">Tanggal Stevedoring</label>
            <input type="date" class="form-control" value="{{$transaksi['tanggal_stevedoring']}}" name="tanggal_stevedoring" id="tanggal_stevedoring"><br>
            <label for="jam_stevedoring">Jam Stevedoring</label>
            <input type="time" class="form-control" value="{{$transaksi['jam_stevedoring']}}" name="jam_stevedoring" id="jam_stevedoring"><br>
            <label for="jumlah_kontainer">Jumlah Kontainer</label>
            <input type="text" class="form-control" value="{{$transaksi['jumlah_kontainer']}}" name="jumlah_kontainer" id="jumlah_kontainer"><br>
            <br>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        <div class="col-4 ms-5">
            <label for="status_stevedoring">Status Stevedoring</label>
            <select name="status_stevedoring" id="status_stevedoring" class="form-select">
                <option selected value=""></option>
                <option <?php if ($transaksi['status_stevedoring'] == "stevedoring 1") echo "selected" ?> value="stevedoring 1">stevedoring 1</option>
                <option <?php if ($transaksi['status_stevedoring'] == "stevedoring 2") echo "selected" ?> value="stevedoring 2">stevedoring 2</option>
            </select><br>
            <label for="lokasi_cy">Lokasi CY</label>
            <input type="text" class="form-control" value="{{$transaksi['lokasi_cy']}}" name="lokasi_cy" id="lokasi_cy"><br>
            <label for="status_cargodoring">Status Cargodoring</label>
            <input type="text" class="form-control" value="{{$transaksi['status_cargodoring']}}" name="status_cargodoring" id="status_cargodoring"><br>
            <label for="tanggal_dr">Tanggal Delivery Receive</label>
            <input type="date" class="form-control" value="{{$transaksi['tanggal_dr']}}" name="tanggal_dr" id="tanggal_dr"><br>
            <label for="jam_dr">Jam Delivery Receive</label>
            <input type="time" class="form-control" value="{{$transaksi['jam_dr']}}" name="jam_dr" id="jam_dr"><br>
            <label for="status_dr">Status Delivery Order</label>
            <select name="status_dr" name="status_dr" id="status_dr" class="form-select">
                <option selected value=""></option>
                <option <?php if ($transaksi['status_dr'] == "dr 1") echo "selected" ?> value="dr 1">DR 1</option>
                <option <?php if ($transaksi['status_dr'] == "dr 2") echo "selected" ?> value="dr 2">DR 2</option>
            </select>
        </div>
    </div>
</form>

@else
@section('title','Tambah Data')
<form action="/add/insert" method="post">
    @csrf
    <div class="row d-flex justify-content-center" style="width: 80vw;padding-top:7vh">
        <div class="col-5 pe-5">
            <label for="tipe">Tipe Transaksi</label>
            <input type="text" class="form-control" placeholder="Tipe Transaksi" name="tipe" id="tipe"><br>
            <label for="nomor_imo">Nomor IMO</label>
            <input type="text" class="form-control" placeholder="Nomor IMO" name="nomor_imo" id="nomor_imo"><br>
            <label for="nama_perusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama_perusahaan" id="nama_perusahaan"><br>
            <label for="tanggal_stevedoring">Tanggal Stevedoring</label>
            <input type="date" class="form-control" placeholder="" name="tanggal_stevedoring" id="tanggal_stevedoring"><br>
            <label for="jam_stevedoring">Jam Stevedoring</label>
            <input type="time" class="form-control" placeholder="" name="jam_stevedoring" id="jam_stevedoring"><br>
            <label for="jumlah_kontainer">Jumlah Kontainer</label>
            <input type="text" class="form-control" placeholder="Jumlah Kontainer" name="jumlah_kontainer" id="jumlah_kontainer"><br>
            <br>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
        <div class="col-5 ps-5">
            <label for="status_stevedoring">Status Stevedoring</label>
            <select name="status_stevedoring" id="status_stevedoring" class="form-select">
                <option value="stevedoring 1">stevedoring 1</option>
                <option value="stevedoring 2">stevedoring 2</option>
                <option selected value=""></option>
            </select><br>
            <label for="lokasi_cy">Lokasi CY</label>
            <input type="text" class="form-control" placeholder="Lokasi CY" name="lokasi_cy" id="lokasi_cy"><br>
            <label for="status_cargodoring">Status Cargodoring</label>
            <input type="text" class="form-control" placeholder="Status Cargodoring" name="status_cargodoring" id="status_cargodoring"><br>
            <label for="tanggal_dr">Tanggal Delivery Receive</label>
            <input type="date" class="form-control" placeholder="" name="tanggal_dr" id="tanggal_dr"><br>
            <label for="jam_dr">Jam Delivery Receive</label>
            <input type="time" class="form-control" placeholder="" name="jam_dr" id="jam_dr"><br>
            <label for="status_dr">Status Delivery Order</label>
            <select name="status_dr" name="status_dr" id="status_dr" class="form-select">
                <option value="dr 1">DR 1</option>
                <option value="dr 2">DR 2</option>
                <option selected value=""></option>
            </select>
        </div>
    </div>
</form>
@endif
@endsection