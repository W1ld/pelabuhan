<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600&display=swap" rel="stylesheet">
    <title>Login</title>
    <style>
        body {
            background: lightgray;
            font-family: 'Quicksand', sans-serif
        }
    </style>
</head>

<body>

    <div class="">
        <div class="row justify-content-center">
            <!-- <div class="card"> -->
            <img class="card-img position-fixed" src="img/dasar.png" alt="Card image">
            <div class="card-img-overlay justify-content-center d-flex align-items-center">
                <div class="col-md-4">
                    <div class="card border-0 shadow rounded">
                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif
                            <h4 class="font-weight-bold text-center">LOGIN</h4>
                            <hr>
                            <form action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="font-weight-bold text-uppercase">Email</label>
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan Email">
                                    @error('email')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="font-weight-bold text-uppercase">Password</label>
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan Password">
                                    @error('password')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="row justify-content-center">
                                    <button type="submit" class="btn btn-primary w-50">LOGIN</button>
                                </div>
                                <hr>

                                <!-- <a href="/forgot-password">Lupa Password ?</a> -->

                            </form>
                            <div class="register pt-3 text-center">
                                <p>Belum punya akun ? Daftar <a href="/register">Disini</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>