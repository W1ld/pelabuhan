<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600&display=swap" rel="stylesheet">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="http://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
  <script type="text/javascript" src="http://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

  <link rel="icon" href="https://files-w.blogspot.com/favicon.ico" type="image/x-icon">
  <title>@yield('title')</title>
  <style>
    body {
      font-family: 'Quicksand', sans-serif;
    }
  </style>
</head>

<body>
  <div class="fixed-top d-flex flex-column flex-shrink-0 p-3 bg-dark bg-gradient" style="width: 222px;height:100vh">
    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-decoration-none" style="color:#0d96fd">
      <!-- <svg class="bi me-2" width="40" height="32">
        <use xlink:href="#bootstrap" />
      </svg> -->
      <span class="fs-2"><b>Doring</b></span>
    </a>
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item">
        <a href="/" class="nav-link text-white {{ request()->is('/') ? 'active' : '' }}">
          Home
        </a>
      </li>
      <li class="nav-item">
        <a href="/data" class="nav-link text-white {{ request()->is('data') ? 'active' : '' }}">
          Input Data
        </a>
      </li>
      <li>
        <a href="/stevedoring" class="nav-link text-white {{ request()->is('stevedoring') ? 'active' : '' }}">
          Stevedoring
        </a>
      </li>
      <li>
        <a href="/cargodoring" class="nav-link text-white {{ request()->is('cargodoring') ? 'active' : '' }}">
          Cargodoring
        </a>
      </li>
      <li>
        <a href="/dr" class="nav-link text-white {{ request()->is('dr') ? 'active' : '' }}">
          Delivery / Receive
        </a>
      </li>
    </ul>
    <hr>
    <div class="dropdown mb-3">
      <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
        <!-- <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2"> -->
        <strong>{{ Auth::user()->name }}</strong>
      </a>
      <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
        <li><a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a></li>
      </ul>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
      @csrf
    </form>
  </div>

  <div class="container" style="margin-left:222px;width:max-content">
    @yield('content')
  </div>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
<script>
$(document).ready(function(){
    $('#Table').dataTable();
});
</script>
</html>