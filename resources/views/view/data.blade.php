@extends('view.template')
@section('title','Input Data')

@section('content')
<a class="btn btn-primary m-4" href="/data/add" role="button">Tambah Data</a>
<table id="Table" class="table table-hover table-striped text-center align-middle pe-3 fw-bolder">
    <thead class="align-middle">
        <th>NO</th>
        <th>ID</th>
        <th>Tipe</th>
        <th>Nomor IMO</th>
        <th>Nama Perusahaan</th>
        <th>Tanggal Stevedoring</th>
        <th>Jam Stevedoring</th>
        <th>Jumlah Kontainer</th>
        <th>Status Stevedoring</th>
        <th>Lokasi CY</th>
        <th>Status Cargodoring</th>
        <th>Tanggal D/R</th>
        <th>Jam D/R</th>
        <th>Status D/R</th>
        <!-- <th>Opsi</th> -->
    </thead>
    <?php $no = 1; ?>
    @foreach($data as $data)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$data->id}}</td>
        <td>{{$data->tipe}}</td>
        <td>{{$data->nomor_imo}}</td>
        <td>{{$data->nama_perusahaan}}</td>
        <td>{{$data->tanggal_stevedoring}}</td>
        <td>{{$data->jam_stevedoring}}</td>
        <td>{{$data->jumlah_kontainer}}</td>
        <td>{{$data->status_stevedoring}}</td>
        <td>{{$data->lokasi_cy}}</td>
        <td>{{$data->status_cargodoring}}</td>
        <td>{{$data->tanggal_dr}}</td>
        <td>{{$data->jam_dr}}</td>
        <td>{{$data->status_dr}}</td>
        <!-- <td>
            <a class="btn btn-primary btn-sm" href="/data/edit/{{$data->id}}" role="button">Edit</a>
            <a class="btn btn-primary btn-sm" href="/data/delete/{{$data->id}}" role="button">Delete</a>
        </td> -->
    </tr>
    @endforeach
</table>

@endsection