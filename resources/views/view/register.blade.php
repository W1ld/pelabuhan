<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600&display=swap" rel="stylesheet">
    <title>Daftar</title>
    <style>
        body {
            background: lightgray;
            font-family: 'Quicksand', sans-serif
        }
    </style>
</head>

<body>

    <div class="">
        <div class="row justify-content-center">
            <img class="card-img position-fixed" src="img/dasar.png" alt="Card image">
            <div class="card-img-overlay justify-content-center d-flex align-items-center">
                <div class="col-md-8">
                    <div class="card border-0 shadow rounded">
                        <div class="card-body">
                            <h4 class="font-weight-bold text-center">REGISTER</h4>
                            <hr>
                            <form action="{{ route('register') }}" method="POST">
                                @csrf

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold text-uppercase" minlength="4" maxlength="16">Nama</label>
                                            <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama">
                                            @error('name')
                                            <div class="alert alert-danger mt-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold text-uppercase">Email</label>
                                            <input type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan email">
                                            @error('email')
                                            <div class="alert alert-danger mt-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold text-uppercase">Password</label>
                                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan Password">
                                            @error('password')
                                            <div class="alert alert-danger mt-2">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="font-weight-bold text-uppercase">Konfirmasi Password</label>
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Masukkan Ulang Password">
                                        </div>
                                    </div>

                                </div>
                                <div class="pb-1 pt-2 row justify-content-center">
                                    <button type="submit" class="btn btn-primary w-25">REGISTER</button>
                                </div>
                            </form>
                            <div class="login mt-3 text-center">
                                <p>Sudah punya akun ? Login <a href="/login">Disini</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>