@extends('view.template')
@section('title','Stevedoring')

@section('content')
<!-- <a class="btn btn-primary my-5 mx-3" href="/add" role="button">Tambah Data</a> -->
<div class="m-4"><h2><b>Delivery / Receive</b></h2></div>
<table id="Table" class="table table-hover table-striped text-center align-middle pe-3 fw-bolder">
    <thead class="align-middle">
        <th>NO</th>
        <th>ID</th>
        <th>Tipe</th>
        <th>Nomor IMO</th>
        <th>Nama Perusahaan</th>
        <!-- <th>Tanggal Stevedoring</th> -->
        <!-- <th>Jam Stevedoring</th> -->
        <!-- <th>Jumlah Kontainer</th> -->
        <!-- <th>Status Stevedoring</th> -->
        <!-- <th>Lokasi CY</th> -->
        <!-- <th>Status Cargodoring</th> -->
        <th>Tanggal D/R</th>
        <th>Jam D/R</th>
        <th>Status D/R</th>
        <th>Opsi</th>
    </thead>
    <?php $no = 1; ?>
    @foreach($data1 as $data1)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$data1->id}}</td>
        <td>{{$data1->tipe}}</td>
        <td>{{$data1->nomor_imo}}</td>
        <td>{{$data1->nama_perusahaan}}</td>
        <!-- <td>{{$data1->tanggal_stevedoring}}</td> -->
        <!-- <td>{{$data1->jam_stevedoring}}</td> -->
        <!-- <td>{{$data1->jumlah_kontainer}}</td> -->
        <!-- <td>{{$data1->status_stevedoring}}</td> -->
        <!-- <td>{{$data1->lokasi_cy}}</td> -->
        <!-- <td>{{$data1->status_cargodoring}}</td> -->
        <td>{{$data1->tanggal_dr}}</td>
        <td>{{$data1->jam_dr}}</td>
        <td>{{$data1->status_dr}}</td>
        <td>
            <a class="btn btn-primary btn-sm" href="/dr/edit/{{$data1->id}}" role="button">Edit</a>
            <!-- <a class="btn btn-primary btn-sm" href="/delete/{{$data1->id}}" role="button">Delete</a> -->
        </td>
        </tr>
        @endforeach
        @foreach($data2 as $data2)
        <tr">
            <td>{{$no++}}</td>
            <td>{{$data2->id}}</td>
            <td>{{$data2->tipe}}</td>
            <td>{{$data2->nomor_imo}}</td>
            <td>{{$data2->nama_perusahaan}}</td>
            <td>{{$data2->tanggal_stevedoring}}</td>
            <td>{{$data2->jam_stevedoring}}</td>
            <td>{{$data2->jumlah_kontainer}}</td>
            <td>{{$data2->status_stevedoring}}</td>
            <!-- <td>{{$data2->lokasi_cy}}</td> -->
            <td>{{$data2->status_cargodoring}}</td>
            <td>{{$data2->tanggal_dr}}</td>
            <td>{{$data2->jam_dr}}</td>
            <td>{{$data2->status_dr}}</td>
            <td>
                <a class="btn btn-primary btn-sm" href="/dr/edit/{{$data2->id}}" role="button">Edit</a>
                <!-- <a class="btn btn-primary btn-sm" href="/delete/{{$data2->id}}" role="button">Delete</a> -->
            </td>
            </tr>
            @endforeach
</table>

@endsection