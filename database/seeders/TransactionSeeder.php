<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'W',
            'email' => 'admin@terminal.doring',
            'password' => Hash::make('w'),
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'nomor_imo' => 'a7gg12d',
            'nama_perusahaan' => 'pt asus campur acer',
            'tanggal_stevedoring' => Carbon::now()->format('Y-m-d'),
            'jam_stevedoring' => Carbon::now()->format('H:i'),
            'jumlah_kontainer' => '123',
            'status_stevedoring' => 'IN PROGRESS',
            'lokasi_cy' => 'Malang',
            'status_cargodoring' => 'FINISH',
            'tanggal_dr' => Carbon::now()->format('Y-m-d'),
            'jam_dr' => Carbon::now()->format('H:i'),
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'nomor_imo' => '9fa41hf',
            'nama_perusahaan' => 'pt lenovo campur dell',
            'tanggal_stevedoring' => Carbon::now()->format('Y-m-d'),
            'jam_stevedoring' => Carbon::now()->format('H:i'),
            'jumlah_kontainer' => '456',
            'status_stevedoring' => 'IN PROGRESS',
            'lokasi_cy' => 'Surabaya',
            'status_cargodoring' => 'IN PROGRESS',
            'tanggal_dr' => Carbon::now()->format('Y-m-d'),
            'jam_dr' => Carbon::now()->format('H:i'),
            'status_dr' => 'FINISH',
        ]);

        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Bongkar',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'ABORT',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'ABORT',
            'status_dr' => 'FINISH',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'ABORT',
        ]);
        DB::table('transactions')->insert([
            'tipe' => 'Muat',
            'status_stevedoring' => 'FINISH',
            'status_cargodoring' => 'FINISH',
            'status_dr' => 'FINISH',
        ]);
    }
}
