<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('tipe')->nullable();
            $table->string('nomor_imo')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->date('tanggal_stevedoring')->nullable();
            $table->string('jam_stevedoring')->nullable();
            $table->integer('jumlah_kontainer')->nullable();
            $table->string('status_stevedoring')->nullable();
            $table->string('lokasi_cy')->nullable();
            $table->string('status_cargodoring')->nullable();
            $table->date('tanggal_dr')->nullable();
            $table->string('jam_dr')->nullable();
            $table->string('status_dr')->nullable();
            // $table->bigInteger('staff_input')->unsigned();
            // $table->foreign('staff_input')->references('id')->on('users')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
