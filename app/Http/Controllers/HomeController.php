<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\transactions;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        $this->transactions = new transactions();
        $data = $this->transactions->getHome();
        return view('view.home', compact('data'));
    }

    public function delete($id)
    {
        $this->transactions = new transactions();
        $this->transactions->deleteData($id);
        return redirect()->route('home');
    }
}
