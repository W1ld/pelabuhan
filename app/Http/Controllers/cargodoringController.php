<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\transactions;

class cargodoringController extends Controller
{
    protected $table = 'transactions';
    public function __construct()
    {
        $this->middleware('auth');
        $this->transactions = new transactions();
    }

    public function __invoke()
    {
        // $data = transactions::all();
        $data1 = $this->transactions->getcd1();
        $data2 = $this->transactions->getcd2();
        return view('view.cargodoring', compact('data1','data2'));
    }

    public function edit($id){
        $transaksi = transactions::find($id);
        if (!$transaksi){
            abort(404);
        }
        return view('forms.cargo', compact('transaksi'));
    }

    public function save($id,Request $request){
        $data = [
            'lokasi_cy' => $request->lokasi_cy,
            'status_cargodoring' => $request->status_cargodoring,
        ];
        $this->transactions->editData($id,$data);
        return redirect()->route('cargodoring');
    }
}
