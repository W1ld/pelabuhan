<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\transactions;

class stevedoringController extends Controller
{
    protected $table = 'transactions';
    public function __construct()
    {
        $this->middleware('auth');
        $this->transactions = new transactions();
    }

    public function __invoke()
    {
        // $data = transactions::where("tipe","Bongkar")->get();
        $data1 = $this->transactions->getsd1();
        $data2 = $this->transactions->getsd2();
        return view('view.stevedoring', compact('data1','data2'));
    }

    public function edit($id){
        $transaksi = transactions::find($id);
        if (!$transaksi){
            abort(404);
        }
        return view('forms.steve', compact('transaksi'));
    }

    public function save($id,Request $request){
        $data = [
            'nomor_imo' => $request->nomor_imo,
            'nama_perusahaan' => $request->nama_perusahaan,
            'tanggal_stevedoring' => $request->tanggal_stevedoring,
            'jam_stevedoring' => $request->jam_stevedoring,
            'jumlah_kontainer' => $request->jumlah_kontainer,
            'status_stevedoring' => $request->status_stevedoring,
            'tanggal_dr' => $request->tanggal_dr,
            'jam_dr' => $request->jam_dr,
            'status_dr' => $request->status_dr,
        ];
        $this->transactions->editData($id,$data);
        return redirect()->route('stevedoring');
    }
}
