<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\transactions;

class dataController extends Controller
{
    protected $table = 'transactions';
    public function __construct()
    {
        $this->middleware('auth');
        $this->transactions = new transactions();
    }

    public function __invoke()
    {
        $data = transactions::all();
        return view('view.data', compact('data'));
    }

    public function add()
    {
        return view('forms.data');
    }

    public function insert(Request $request)
    {
        $data = [
            'tipe' => $request->tipe,
            'nomor_imo' => $request->nomor_imo,
            'nama_perusahaan' => $request->nama_perusahaan,
            'tanggal_stevedoring' => $request->tanggal_stevedoring,
            'jam_stevedoring' => $request->jam_stevedoring,
            'jumlah_kontainer' => $request->jumlah_kontainer,
            'status_stevedoring' => $request->status_stevedoring,
            'lokasi_cy' => $request->lokasi_cy,
            'status_cargodoring' => $request->status_cargodoring,
            'tanggal_dr' => $request->tanggal_dr,
            'jam_dr' => $request->jam_dr,
            'status_dr' => $request->status_dr,
        ];
        $this->transactions->addData($data);
        return redirect()->route('data');
    }
}
