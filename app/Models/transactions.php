<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class transactions extends Model
{
    use HasFactory;
    protected $fillable = [
        'tipe',
        'nomor_imo',
        'nama_perusahaan',
        'tanggal_stevedoring',
        'jam_stevedoring',
        'jumlah_kontainer',
        'status_stevedoring',
        'lokasi_cy',
        'status_cargodoring',
        'tanggal_dr',
        'jam_dr',
        'status_dr',
    ];
    public function addData($data)
    {
        return DB::table('transactions')->insert($data);
    }

    public function editData($id,$data)
    {
        return DB::table('transactions')->where('id', $id)->update($data);
    }

    public function deleteData($id)
    {
        return DB::table('transactions')->where('id', $id)->delete();
    }

    public function getHome()
    {
        return DB::table('transactions')->where("status_stevedoring","FINISH")->where("status_dr","FINISH")->where("status_cargodoring","FINISH")->get();
    }
    public function getsd1()
    {
        // return DB::table('transactions')->where("tipe","Bongkar")->where("status_stevedoring","!=","FINISH")->orWhere("status_stevedoring", null)->get();
        return DB::table('transactions')->where("tipe","Bongkar")->where(function ($query) {
                $query->where('status_stevedoring', '!=', 'FINISH')
                      ->orWhere('status_stevedoring', null);
            })->get();
    }
    public function getsd2()
    {
        return DB::table('transactions')->where("tipe","Muat")->where("status_cargodoring","FINISH")->where(function ($query) {
            $query->where('status_stevedoring', '!=', 'FINISH')
                  ->orWhere('status_stevedoring', null);
        })->get();
    }
    public function getdr1()
    {
        // return DB::table('transactions')->where("tipe","Bongkar")->where("status_stevedoring","FINISH")->where("status_cargodoring","FINISH")->get();
        return DB::table('transactions')->where("tipe","Bongkar")->where('status_cargodoring', 'FINISH')->where(function ($query) {
            $query->where('status_dr', '!=', 'FINISH')
                  ->orWhere('status_dr', null);
        })->get();
    }
    public function getdr2()
    {
        // return DB::table('transactions')->where("tipe","Muat")->where("status_dr","!=","FINISH")->orWhere("status_dr", null)->get();
        return DB::table('transactions')->where("tipe","Muat")->where(function ($query) {
            $query->where('status_dr', '!=', 'FINISH')
                  ->orWhere('status_dr', null);
        })->get();
    }
    public function getcd1()
    {
        // return DB::table('transactions')->where("tipe", "Bongkar")->where("status_stevedoring", "FINISH")->where("status_cargodoring", "!=", "FINISH")->orWhere("status_cargodoring", null)->get();
        return DB::table('transactions')->where("tipe", "Bongkar")->where("status_stevedoring", "FINISH")->where(function ($query) {
            $query->where('status_cargodoring', '!=', 'FINISH')
                  ->orWhere('status_cargodoring', null);
        })->get();
    }
    public function getcd2()
    {
        // return DB::table('transactions')->where("tipe", "Muat")->where("status_dr", "=", "FINISH")->where("status_cargodoring", "!=", "FINISH")->orWhere("status_cargodoring", null)->get();
        return DB::table('transactions')->where("tipe", "Muat")->where("status_dr", "=", "FINISH")->where(function ($query) {
            $query->where('status_cargodoring', '!=', 'FINISH')
                  ->orWhere('status_cargodoring', null);
        })->get();
    }
}